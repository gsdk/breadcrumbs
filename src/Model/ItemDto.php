<?php

namespace Sdk\Breadcrumbs\Model;

class ItemDto
{
    public function __construct(
        public readonly string $text,
        public readonly ?string $href = null,
        public readonly ?string $id = null,
        public readonly ?string $title = null,
        public readonly ?string $target = null,
        public readonly ?string $class = null,
    ) {
    }

    public static function createFromArray(array $data): ItemDto
    {
        return new ItemDto(
            text: $data['text'] ?? '',
            href: $data['href'] ?? $data['url'] ?? null,
            id: $data['id'] ?? null,
            title: $data['title'] ?? null,
            target: $data['target'] ?? null,
            class: $data['class'] ?? $data['cls'] ?? null
        );
    }

    public static function createFromMixed(array|string|ItemDto $data): ItemDto
    {
        if (is_string($data)) {
            return new ItemDto($data);
        } elseif (is_array($data)) {
            return static::createFromArray($data);
        } else {
            return $data;
        }
    }
}