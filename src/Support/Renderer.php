<?php

namespace Sdk\Breadcrumbs\Support;

use Sdk\Breadcrumbs\BreadcrumbsBuilder;
use Sdk\Breadcrumbs\Model\ItemDto;

class Renderer
{
    protected string $separator = '<div class="separator"></div>';

    public function __construct(private readonly ?string $view)
    {
    }

    public function render(BreadcrumbsBuilder $builder): string
    {
        if ($builder->isEmpty()) {
            return '';
        }

        if (isset($this->view)) {
            $items = [];
            $items[] = $builder->getHome();
            $items = array_merge($items, $builder->getItems());

            return (string)view($this->view, [
                'breadcrumbs' => $this,
                'items' => array_filter($items)
            ]);
        }

        return '<div class="breadcrumbs"><nav>'
            . $this->renderItems($builder)
            . '</nav></div>';
    }

    protected function renderItems(BreadcrumbsBuilder $builder): string
    {
        $menu = [];

        if ($builder->getHome()) {
            $menu[] = $this->renderItem($builder->getHome());
        }

        foreach ($builder->getItems() as $item) {
            $menu[] = $this->renderItem($item);
        }

        return implode($this->separator, $menu);
    }

    protected function renderItem(ItemDto $item): string
    {
        $tag = $item->href ? 'a' : 'div';
        $html = '<' . $tag;
        $attributes = ['id', 'title', 'target', 'href', 'class'];
        $html .= array_reduce($attributes, function ($s, $k) use ($item) {
            return $s . ($item->$k ? ' ' . $k . '="' . $item->$k . '"' : '');
        }, '');
        $html .= '>';
        $html .= $item->text;
        $html .= '</' . $tag . '>';

        return $html;
    }
}