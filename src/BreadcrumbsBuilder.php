<?php

namespace Sdk\Breadcrumbs;

use Sdk\Breadcrumbs\Model\ItemDto;
use Sdk\Breadcrumbs\Support\Renderer;

class BreadcrumbsBuilder
{
    protected array $items = [];

    protected ?ItemDto $homeItem = null;

    protected string $view;

    public function __construct()
    {
        $this->build();
    }

    public function view(string $view): static
    {
        $this->view = $view;

        return $this;
    }

    public function isEmpty(): bool
    {
        return empty($this->items);
    }

    public function add(array|string|ItemDto $data): static
    {
        $this->items[] = ItemDto::createFromMixed($data);

        return $this;
    }

    public function addUrl(string $url, array|string $params): static
    {
        if (is_string($params)) {
            $params = ['text' => $params];
        }

        return $this->add(array_merge($params, ['url' => $url]));
    }

    public function addRoute(string $route, array|string $params): static
    {
        if (is_string($params)) {
            $params = ['text' => $params];
        }

        return $this->add(
            array_merge($params, [
                'id' => $route,
                'url' => route($route)
            ])
        );
    }

    public function addHome(string $url, array|string $params): static
    {
        if (is_string($params)) {
            $params = ['text' => $params];
        }

        $this->homeItem = ItemDto::createFromMixed(array_merge($params, ['url' => $url]));

        return $this;
    }

    public function getHome(): ?ItemDto
    {
        return $this->homeItem;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function render(): string
    {
        return (new Renderer($this->view ?? null))->render($this);
    }

    public function __toString(): string
    {
        return $this->render();
    }

    protected function build()
    {
    }
}
